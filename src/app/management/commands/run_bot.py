import logging

from django.core.management import BaseCommand
from django.conf import settings
from telegram.ext import Updater
from ...internal.transport.bot.handlers import start_handler, set_phone_handler, me_handler


class Command(BaseCommand):
    def handle(self, *args, **options):
        updater = Updater(token=settings.TOKEN, use_context=True)
        dispatcher = updater.dispatcher
        logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                            level=logging.INFO)
        dispatcher.add_handler(start_handler)
        dispatcher.add_handler(set_phone_handler)
        dispatcher.add_handler(me_handler)
        updater.start_polling()
