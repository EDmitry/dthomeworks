# Generated by Django 3.2.5 on 2022-02-11 13:41

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='AppUser',
            fields=[
                ('id', models.CharField(blank=True, max_length=15, primary_key=True, serialize=False)),
                ('telegram_adr', models.CharField(blank=True, max_length=255, unique=True)),
                ('first', models.CharField(blank=True, max_length=255)),
                ('last', models.CharField(blank=True, max_length=255)),
                ('lang', models.CharField(blank=True, max_length=2)),
                ('tel', models.CharField(blank=True, max_length=17, unique=True, validators=[django.core.validators.RegexValidator(message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.", regex='^\\+?1?\\d{9,15}$')])),
            ],
        ),
    ]
