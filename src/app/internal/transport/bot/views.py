from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from ...models.app_user import AppUser
from ...transport.bot.app_user_serializer import AppUserSerializer


@api_view(['GET'])
def app_user_detail(request, id, format = None):
    try:
        app_user = AppUser.objects.get(external_id=id)
    except AppUser.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)
    serializer = AppUserSerializer(app_user)
    return Response(serializer.data)