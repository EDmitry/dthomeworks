import re

from telegram import Update
from telegram.ext import CallbackContext, CommandHandler

from ...services.bot.filters import filter_phone_number
from ...models.app_user import AppUser


def start(update: Update, context: CallbackContext):
    user = update['message']['from_user']
    AppUser.objects.get_or_create(external_id=user['id'], username=user['username'],
                                                      first_name=user['first_name'], last_name=user['last_name'],
                                                      language_code=user['language_code'])
    context.bot.send_message(chat_id=update.effective_chat.id, text="Hello {}!".format(user['first_name']))


start_handler = CommandHandler('start', start)


def set_phone(update: Update, context: CallbackContext):
    pattern = re.compile("^\+?1?\d{9,15}$")
    app_user = AppUser.objects.filter(username=update['message']['from_user']['username'])[0]
    if len(context.args) != 1 or not pattern.match(context.args[0]):
        context.bot.send_message(chat_id=update.effective_chat.id, text="Wrong format! Phone number must be entered "
                                                                        "in the format: '/set_phone +999999999'. Up "
                                                                        "to 15 digits allowed.")
    else:
        app_user.tel = context.args[0]
        app_user.save()
        context.bot.send_message(chat_id=update.effective_chat.id, text='Your phone number successfully selected.')


set_phone_handler = CommandHandler('set_phone', set_phone)


def me(update: Update, context: CallbackContext):
    app_user = AppUser.objects.filter(username=update['message']['from_user']['username'])[0]
    text = 'id: {}\nusername: @{}\nfirst name: {}\nlast name: {}\nlanguage: {}\nphone number: {}'.format(
        app_user.external_id,
        app_user.username,
        app_user.first_name,
        app_user.last_name,
        app_user.language_code,
        app_user.tel)
    context.bot.send_message(chat_id=update.effective_chat.id, text=text)


me_handler = CommandHandler('me', me, filters=filter_phone_number)