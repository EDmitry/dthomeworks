from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from .transport.bot import views

urlpatterns = [
    path('me/<int:id>', views.app_user_detail),
]

urlpatterns = format_suffix_patterns(urlpatterns)
