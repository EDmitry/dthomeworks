from telegram.ext import MessageFilter
from ...models.app_user import AppUser


class FilterPhoneNumber(MessageFilter):
    def filter(self, message):
        app_user = AppUser.objects.filter(external_id=message.from_user.id)
        if len(app_user) > 0:
            return app_user[0].tel != ""
        else:
            return False


# Remember to initialize the class.
filter_phone_number = FilterPhoneNumber()
