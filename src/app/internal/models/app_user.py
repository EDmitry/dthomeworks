from django.core.validators import RegexValidator
from django.db import models


class AppUser(models.Model):
    external_id = models.PositiveIntegerField(unique=True, default=None)
    username = models.CharField(unique=True, max_length=255, default=None)
    first_name = models.CharField(max_length=255, default=None)
    last_name = models.CharField(max_length=255, default=None)
    language_code = models.CharField(max_length=2, default=None)
    tel_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$',
                               message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")
    tel = models.CharField(unique=True, validators=[tel_regex], max_length=17, blank=True)  # validators should be a list

    class Meta:
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'
